package com.mpp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@Controller
public class BasicController {
    @RequestMapping("")
    @ResponseBody
    public String test() {
        return "";
    }
}
