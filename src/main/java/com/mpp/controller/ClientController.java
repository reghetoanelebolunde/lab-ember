package com.mpp.controller;


import com.mpp.domain.Client;
import com.mpp.dto.ClientDto;
import com.mpp.dto.ClientListDto;
import com.mpp.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Tudor Gergely, Catalysts GmbH.
 */
@RestController
@RequestMapping("/api")
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(final ClientService clientService) {
        this.clientService = clientService;
    }

    /**
     * Find all clients
     * @return list of all clients
     */
    @RequestMapping("/clients")
    public ClientListDto findAll() {
        return new ClientListDto(clientService.findAllSQL());
    }

    /**
     * Find the client with the specified id
     * @param id id of client
     * @return Client
     */
    @RequestMapping("/clients/{id}")
    public ClientDto findById(@PathVariable final Integer id) {
        return new ClientDto(clientService.findOne(id));
    }

    /**
     * Saves a new client
     * @param client the client to be saved
     * @return Saved Client
     */
    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public Client save(@RequestBody final ClientDto client) {
        return clientService.save(client.getClient().getName());
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    public void removeById(@PathVariable final Integer id) {
        clientService.removeById(id);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    public void updateById(@PathVariable final Integer id, @RequestBody final ClientDto client) {
        clientService.updateById(id, client.getClient().getName());
    }
}

