package com.mpp.controller;

import com.mpp.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.mpp.service.MovieService;

import java.util.List;

/**
 * @author Tudor Gergely, Catalysts GmbH.
 */
@RestController
public class MovieController {
    private final MovieService movieService;

    @Autowired
    public MovieController(final MovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * Returns all movies
     * @return List<Movie> all movies
     */
    @RequestMapping("/movie")
    public List<Movie> findAll() {
        return movieService.findAll();
    }

    @RequestMapping("/movie/{movieCategoryName}")
    public List<Movie> findByCategoryName(@PathVariable final String movieCategoryName) {
        return movieService.findByCategoryName(movieCategoryName);
    }

    @RequestMapping("/movie/{id}")
    public Movie findById(@PathVariable final Integer id) {
        return movieService.findById(id);
    }

    @RequestMapping(value = "/movie", method = RequestMethod.POST)
    public Movie save(@RequestBody final Movie movie) {
        return movieService.save(movie.getTitle(), movie.getMovieCategory());
    }

    /**
     * Remove a movie by id
     * @param id movie id
     */
    @RequestMapping(value = "/movie/{id}", method = RequestMethod.DELETE)
    public void removeById(@PathVariable final Integer id) {
        movieService.removeById(id);
    }

    /**
     * Update a movie
     * @param id id of the movie
     * @param n new title of the movie
     */
    @RequestMapping(value = "/movie/{id}", method = RequestMethod.PUT)
    public void updateById(@PathVariable final Integer id, @RequestBody final Movie movie) {
        movieService.update(id, movie);
    }

    @RequestMapping("/movie/most-rented")
    public List<Movie> findMostRentedMovies() {
        return movieService.findMostRentedMovies();
    }
}
