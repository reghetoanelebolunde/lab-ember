package com.mpp.controller;

import com.mpp.domain.Rental;
import com.mpp.domain.validator.RentalInvalidException;
import com.mpp.domain.validator.RentalValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.mpp.service.RentalService;

import java.util.List;

/**
 * @author Tudor Gergely, Catalysts GmbH.
 */
@RestController
public final class RentalController {
    private final RentalService rentalService;
    private final RentalValidator rentalValidator;

    @Autowired
    public RentalController(final RentalService rentalService, final RentalValidator rentalValidator) {
        this.rentalService = rentalService;
        this.rentalValidator = rentalValidator;
    }

    @RequestMapping("/rental")
    public List<Rental> findAll() {
        return rentalService.findAll();
    }

    @RequestMapping("/rental/{id}")
    public Rental findById(@PathVariable final Integer id) {
        return rentalService.findById(id);
    }

    @RequestMapping(value = "/rental", method = RequestMethod.POST)
    public Rental save(@RequestBody final Rental rental) throws RentalInvalidException {
        rentalValidator.validate(rental);
        return rentalService.save(rental);
    }

    @RequestMapping(value = "/rental/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Integer id) {
        rentalService.remove(id);
    }
}
