package com.mpp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by David on 3/7/2016.
 */
@NamedEntityGraphs({
        @NamedEntityGraph(name = "clientsSimple", attributeNodes = {
                @NamedAttributeNode(value = "name"),
                @NamedAttributeNode(value = "rentals", subgraph = "clientRentals")
        }, subgraphs = {
                @NamedSubgraph(name = "clientRentals", attributeNodes = {
                        @NamedAttributeNode(value = "movie")
                })
        }
        )
})
@Entity
@Table(name = "clients")
public class Client implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private List<Rental> rentals;

    public Client(String name) {
        this.name = name;
    }

    public Client(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Client() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return id + "." + name;
    }

    public List<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }
}
