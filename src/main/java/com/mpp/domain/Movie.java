package com.mpp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by David on 3/7/2016.
 */
@Entity
@Table(name = "movies")
public class Movie implements Serializable {
    @Id
    private Integer id;

    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    private MovieCategory movieCategory;

    @OneToMany(mappedBy = "movie")
    private List<Rental> rentals;

    public Movie(){}

    public Movie(Integer id, String title, MovieCategory movieCategory) {
        this.id = id;
        this.title = title;
        this.movieCategory = movieCategory;
    }

    public Movie(String title, MovieCategory movieCategory) {
        this.title = title;
        this.movieCategory = movieCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MovieCategory getMovieCategory() {
        return movieCategory;
    }

    public void setMovieCategory(MovieCategory movieCategory) {
        this.movieCategory = movieCategory;
    }

    public List<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    @Override
    public String toString() {
        return id + " " + title + ' ' + movieCategory;
    }
}
