package com.mpp.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

/**
 * Created by David on 3/7/2016.
 */
@Entity
@Table(name = "movie_categories")
public class MovieCategory implements Serializable {
    @Id
    private Integer id;
    private String categoryName;

    @OneToMany(mappedBy = "movieCategory")
    private List<Movie> movies;

    public MovieCategory() {}

    public MovieCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
