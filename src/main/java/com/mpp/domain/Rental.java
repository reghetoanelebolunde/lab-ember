package com.mpp.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by David on 3/7/2016.
 */
@Entity
@Table(name = "rentals")
public class Rental implements Serializable {
    @Id
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="movie_id")
    private Movie movie;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="client_id")
    private Client client;

    public Rental() {
    }

    public Rental(Integer id, Movie movie, Client client) {
        this.id = id;
        this.movie = movie;
        this.client = client;
    }

    public Rental(Movie movie, Client client) {
        this.movie = movie;
        this.client = client;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String toString() {
        return client + " rented movie " + movie;
    }
}
