package com.mpp.domain.validator;

import com.mpp.domain.Rental;
import com.mpp.service.ClientService;
import com.mpp.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author tudor.gergely on 3/15/2016
 */
@Component
public final class RentalValidator {
    private final ClientService clientService;
    private final MovieService movieService;

    @Autowired
    public RentalValidator(final ClientService clientService, final MovieService movieService) {
        this.clientService = clientService;
        this.movieService = movieService;
    }

    /**
     * Validate the entity
     * @param rental entity to be validated
     * @throws RentalInvalidException when entity is invalid
     */
    public void validate(final Rental rental) throws RentalInvalidException {
        try {
            clientService.findOne(rental.getClient().getId());
            movieService.findById(rental.getMovie().getId());
        } catch (RuntimeException e) {
            throw new RentalInvalidException();
        }
    }
}
