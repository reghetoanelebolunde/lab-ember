package com.mpp.dto;

import com.mpp.domain.Client;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
public class ClientDto {
    private ClientIdDto client;

    public ClientDto() {
        
    }

    public ClientDto(Client client) {
        this.client = new ClientIdDto(client);
    }

    public ClientIdDto getClient() {
        return client;
    }

    public void setClient(ClientIdDto client) {
        this.client = client;
    }
}
