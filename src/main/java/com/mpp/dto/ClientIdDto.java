package com.mpp.dto;

import com.mpp.domain.Client;
import com.mpp.domain.Rental;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
public class ClientIdDto {
    private Integer id;
    private String name;
    private List<Integer> rentals;

    public ClientIdDto(final Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.rentals = client.getRentals().stream().map(Rental::getId).collect(Collectors.toList());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getRentals() {
        return rentals;
    }

    public void setRentals(List<Integer> rentals) {
        this.rentals = rentals;
    }
}
