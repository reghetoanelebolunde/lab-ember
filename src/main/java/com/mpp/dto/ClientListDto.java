package com.mpp.dto;

import com.mpp.domain.Client;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
public class ClientListDto {
    private List<ClientIdDto> clients;

    public ClientListDto(List<Client> clients) {
        this.clients = clients.stream().map(ClientIdDto::new).collect(Collectors.toList());
    }

    public List<ClientIdDto> getClients() {
        return clients;
    }

    public void setClients(List<ClientIdDto> clients) {
        this.clients = clients;
    }
}
