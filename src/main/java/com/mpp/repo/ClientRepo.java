package com.mpp.repo;

import com.mpp.domain.Client;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@Repository
public interface ClientRepo extends JpaRepository<Client, Integer> {

    @Query("select c from Client c")
    @EntityGraph(value = "clientsSimple", type = EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllGraph();
}
