package com.mpp.repo;

import com.mpp.domain.MovieCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@Repository
public interface MovieCategoryRepo extends JpaRepository<MovieCategory, Integer> {
}
