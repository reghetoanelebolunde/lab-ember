package com.mpp.repo;

import com.mpp.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@Repository
public interface MovieRepo extends JpaRepository<Movie, Integer> {
    @Query("SELECT movie FROM Movie movie where movie.movieCategory.categoryName = :categoryName")
    List<Movie> findByCategoryName(String categoryName);

//    List<Movie> mostRentedMovies();
}
