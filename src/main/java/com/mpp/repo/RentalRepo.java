package com.mpp.repo;

import com.mpp.domain.Rental;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@Repository
public interface RentalRepo extends JpaRepository<Rental, Integer> {
}
