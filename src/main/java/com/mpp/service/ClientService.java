package com.mpp.service;

import com.mpp.domain.Client;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tudor.gergely on 3/8/2016
 */
@Service
public interface ClientService {
    List<Client> findAll();

    List<Client> findAllSQL();

    List<Client> findAllJPACriteria();

    List<Client> findAllNamedEntityQ();

    List<Client> findAllJpql();

    Client findOne(Integer id);

    Client save(String name);

    void removeById(Integer id);

    void updateById(Integer id, String n);
}
