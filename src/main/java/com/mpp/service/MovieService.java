package com.mpp.service;

import com.mpp.domain.Movie;
import com.mpp.domain.MovieCategory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tudor.gergely on 3/7/2016
 */
@Service
public interface MovieService {
    List<Movie> findAll();

    Movie findById(Integer id);

    List<Movie> findByCategoryName(String name);

    Movie save(String title, MovieCategory movieCategory);

    void update(Integer id, Movie movie);

    void removeById(Integer id);

    List<Movie> findMostRentedMovies();
}
