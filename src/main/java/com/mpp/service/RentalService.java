package com.mpp.service;

import com.mpp.domain.Rental;
import com.mpp.domain.validator.RentalInvalidException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PGhiran on 3/15/2016.
 */
@Service
public interface RentalService {
    List<Rental> findAll();

    Rental findById(Integer id);

    Rental save(Rental rental) throws RentalInvalidException;

    void remove(Integer rental);
}
