package com.mpp.service.impl;

import com.mpp.domain.Client;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.mpp.repo.ClientRepo;
import com.mpp.service.ClientService;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author tudor.gergely on 3/8/2016
 */
@Component
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientRepo clientRepo;

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Client> findAll() {
        return clientRepo.findAll();
    }

    @Override
    public List<Client> findAllSQL() {
        Query query = ((Session)em.getDelegate()).createSQLQuery("select * from clients");
        return query.list();
    }

    @Override
    public List<Client> findAllJPACriteria() {
        Criteria crit = ((Session)em.getDelegate())
                .createCriteria(Client.class);

        return crit.list();
    }

    @Override
    public List<Client> findAllNamedEntityQ() {
        return clientRepo.findAllGraph();
    }

    @Override
    public List<Client> findAllJpql() {
        javax.persistence.Query query = em.createQuery("select c from Client c");
        return query.getResultList();
    }

    @Override
    public Client findOne(final Integer id) {
        return clientRepo.findOne(id);
    }

    @Override
    public Client save(String name) {
        return clientRepo.save(new Client(name));
    }

    @Override
    public void removeById(final Integer id) {
        clientRepo.delete(id);
    }

    @Override
    public void updateById(final Integer id, final String n) {
        final Client client = clientRepo.findOne(id);
        client.setName(n);
        clientRepo.save(client);
    }
}
