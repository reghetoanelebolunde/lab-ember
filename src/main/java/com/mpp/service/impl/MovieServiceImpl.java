package com.mpp.service.impl;

import com.mpp.domain.Movie;
import com.mpp.domain.MovieCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.mpp.repo.MovieCategoryRepo;
import com.mpp.repo.MovieRepo;
import com.mpp.service.MovieService;
import com.mpp.service.RentalService;

import java.util.List;

/**
 * @author tudor.gergely on 3/7/2016
 */
@Component
public final class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepo movieRepo;
    @Autowired
    private MovieCategoryRepo movieCategoryRepo;
    @Autowired
    private RentalService rentalService;

    @Override
    public List<Movie> findAll() {
        return movieRepo.findAll();
    }

    @Override
    public Movie findById(final Integer id) {
        return movieRepo.findOne(id);
    }

    @Override
    public List<Movie> findByCategoryName(final String name) {
        return movieRepo.findByCategoryName(name);
    }

    @Override
    public Movie save(String title, MovieCategory movieCategory) {
        return movieRepo.save(new Movie(title, movieCategory));
    }

    @Override
    public void update(final Integer id, final Movie movie) {
        final Movie m = movieRepo.findOne(movie.getId());
        if (m != null) {
            movieRepo.save(movie);
        }
    }

    @Override
    public void removeById(final Integer id) {
        movieRepo.delete(id);
    }

    /**
     * Finds the most rented movies
     * @return List of most rented movies
     */
    @Override
    public List<Movie> findMostRentedMovies() {
//        return movieRepo.mostRentedMovies();
        return null;
    }
}
