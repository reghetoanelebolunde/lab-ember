package com.mpp.service.impl;

import com.mpp.domain.Rental;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.mpp.repo.RentalRepo;
import com.mpp.service.RentalService;

import java.util.List;

/**
 * Created by PGhiran on 3/15/2016.
 */
@Component
public final class RentalServiceImpl implements RentalService {
    @Autowired
    private RentalRepo rentalRepo;

    @Override
    public List<Rental> findAll() {
        return rentalRepo.findAll();
    }

    @Override
    public Rental findById(final Integer id) {
        return rentalRepo.findOne(id);
    }


    @Override
    public Rental save(final Rental rental) {
        return rentalRepo.save(rental);
    }

    /**
     * Removes a rental
     *
     * @param id to be removed
     */
    @Override
    public void remove(final Integer id) {
        rentalRepo.delete(id);
    }
}
