import Ember from 'ember';

export default Ember.Component.extend({
  name: null,
  actions: {
    save() {
      console.log(this.get('name'));
      this.sendAction('save', { name: this.get('name') });
      this.set('name', null);
    }
  }
});
