import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    delete() {
      console.log(this.get('client.id'));
      this.sendAction('delete', this.get('client.id'));
    },
    update() {
      this.sendAction('update', this.get('client'));
    }
  }
});
