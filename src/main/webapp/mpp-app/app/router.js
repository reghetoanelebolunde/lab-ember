import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('client');
  this.route('movie');
  this.route('rental');
});

export default Router;
