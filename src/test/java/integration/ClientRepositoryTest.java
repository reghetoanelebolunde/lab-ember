package integration;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mpp.Application;
import com.mpp.domain.Client;
import com.mpp.repo.ClientRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;
import java.util.Objects;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
@ActiveProfiles("local")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/sampleClient.xml")
public class ClientRepositoryTest {
    private final Logger LOG = LoggerFactory.getLogger(Application.class);

    @Autowired
    ClientRepo clientRepo;

    @Test
    public void testFindAllGraph() {
        LOG.info("Testing client repo integration");
        final List<Client> result = clientRepo.findAllGraph();

        assert result.size() == 1;
        assert result.get(0).getId() == 1;
        assert Objects.equals(result.get(0).getName(), "name");
        LOG.info("Client repo integration {}", "Success!!");
    }
}
