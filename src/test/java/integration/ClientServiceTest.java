package integration;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.mpp.Application;
import com.mpp.domain.Client;
import com.mpp.service.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
@ActiveProfiles("local")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/sampleClient.xml")
@Transactional
public class ClientServiceTest {
    private final Logger LOG = LoggerFactory.getLogger(Application.class);

    @Autowired
    ClientService clientService;

    @Test
    public void testFindAll() {
        LOG.info("testing find all");

        List<Client> all = clientService.findAll();
        assert all.size() == 1;

        LOG.info("find all success");
    }

    @Test
    public void testFindAllSql() {
        LOG.info("testing find all");

        List<Client> all = clientService.findAllSQL();
        assert all.size() == 1;

        LOG.info("find all success");
    }

    @Test
    public void testFindAllJpql() {
        LOG.info("testing find all");

        List<Client> all = clientService.findAllJpql();
        assert all.size() == 1;

        LOG.info("find all success");
    }

    @Test
    public void testFindAllJpaCriteria() {
        LOG.info("testing find all");

        List<Client> all = clientService.findAllJPACriteria();
        assert all.size() == 1;

        LOG.info("find all success");
    }

    @Test
    public void testFindAllNamed() {
        LOG.info("testing find all");

        List<Client> all = clientService.findAllNamedEntityQ();
        assert all.size() == 1;

        LOG.info("find all success");
    }

    @Test
    public void testFindOne() {
        LOG.debug("test find one");

        final Client client = clientService.findOne(1);

        assert client.getId() == 1;
        assert client.getName().equals("name");
    }

    @Test
    public void testSave() {
        LOG.info("testing save");
        Client client = clientService.save("test");

        assert client.getName().equals("test");
        assert clientService.findAll().size() == 2;
        LOG.info("testing save success");
    }

    @Test
    public void testRemoveById() {
        LOG.info("testing remove by id: {}", 1);
        assert clientService.findAll().size() == 1;

        clientService.removeById(1);

        assert clientService.findAll().size() == 0;
    }

    @Test
    public void testUpdateById() {
        LOG.error("Testing update by id");

        assert clientService.findOne(1).getName().equals("name");

        clientService.updateById(1, "test");

        assert clientService.findOne(1).getName().equals("test");

        LOG.debug("Update by id success");
    }
}
