package unit;

/**
 * @author tudorgergely, Catalysts GmbH.
 */

import com.mpp.Application;
import com.mpp.controller.ClientController;
import com.mpp.domain.Client;
import com.mpp.dto.ClientDto;
import com.mpp.dto.ClientListDto;
import com.mpp.service.ClientService;
import org.junit.Before;

import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.ClientFactory;

import static org.junit.Assert.*;

public class ClientControllerTest {
    private final Logger LOG = LoggerFactory.getLogger(Application.class);

    @InjectMocks
    private ClientController clientController;

    @Mock
    private ClientService clientService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMockCreation() {
        assertNotNull(clientController);
        assertNotNull(clientService);
    }

    @Test
    public void testFindAll() {
        final ClientListDto clientListDto = ClientFactory.getClientListDto();

//        when(clientService.findAll()).thenReturn(clientListDto.getClients());

        final ClientListDto result = clientController.findAll();

        assertTrue(clientListDto.getClients().containsAll(result.getClients()));
        assertTrue(result.getClients().containsAll(clientListDto.getClients()));
    }

    @Test
    public void testFindById() {
        final ClientDto clientDto = ClientFactory.getClientDto();

//        when(clientService.findOne(1)).thenReturn(clientDto.getClient());

        final ClientDto result = clientController.findById(1);

        assertEquals(clientDto.getClient(), result.getClient());
    }

    @Test
    public void testSave() {
        final ClientDto clientDto = ClientFactory.getClientDto();

//        when(clientService.save("test")).thenReturn(clientDto.getClient());

        final Client result = clientController.save(clientDto);

        assertEquals(result, clientDto.getClient());
    }

    @Test
    public void testRemoveById() {
        clientController.removeById(1);

        verify(clientService, times(1)).removeById(1);
    }

    @Test
    public void testUpdateById() {
        LOG.warn("testing warn log with update by id controller test");
        final ClientDto clientDto = ClientFactory.getClientDto();

        clientController.updateById(1, clientDto);

        verify(clientService, times(1)).updateById(1, clientDto.getClient().getName());
        LOG.error("update by id success!!, id: {}", 1);
    }
}
