package util;

import com.mpp.domain.Client;
import com.mpp.dto.ClientDto;
import com.mpp.dto.ClientListDto;

import java.util.Collections;
import java.util.List;

/**
 * @author tudorgergely, Catalysts GmbH.
 */
public class ClientFactory {
    public static Client getClient() {
        return new Client(1, "test");
    }

    public static List<Client> getClientList() {
        return Collections.singletonList(getClient());
    }

    public static ClientListDto getClientListDto() {
        return new ClientListDto(getClientList());
    }

    public static ClientDto getClientDto() {
        return new ClientDto(getClient());
    }
}
