import DS from "ember-data";

const ApplicationAdapter = DS.RESTAdapter.extend({
  host: 'http://localhost:8080',
  namespace: 'api'
});

export default ApplicationAdapter;
