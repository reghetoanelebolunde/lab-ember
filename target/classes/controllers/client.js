import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    save(client) {
      const c = this.store.createRecord('client', client);
      console.log(c);
      c.save();
    },
    delete(id) {
      console.log(id);
      this.store.findRecord('client', id).then(client => {
        client.destroyRecord();
        this.store.unloadRecord(client);
      });
    },
    update(c) {
      console.log(c.id, c.get('name'));
      this.store.findRecord('client', c.id).then(function(client) {
        client.set('name' , c.get('name'));
        client.save();
      });
    }
  }
});
